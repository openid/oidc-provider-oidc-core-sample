FROM node:14-alpine
ENV NODE_ENV production
ENV DEBUG oidc-provider:*
ENV NODE_TLS_REJECT_UNAUTHORIZED 0
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./
USER node
RUN npm install --production --ignore-scripts
COPY --chown=node:node . .
