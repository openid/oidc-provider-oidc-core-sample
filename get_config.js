const { readFileSync, existsSync } = require('fs')

let CONFIG

if (!existsSync('config.json')) {
  console.error('config.json file missing')
  process.exit(1)
}

try {
  CONFIG = JSON.parse(readFileSync('config.json'))
} catch (err) {
  console.error('failed to parse config.json')
  process.exit(1)
}

if (!('port' in CONFIG)) {
  console.error('"port" configuration missing in config.json')
  process.exit(1)
}

if (typeof CONFIG.port !== 'number') {
  console.error('"port" configuration must be a number')
  process.exit(1)
}

if (!('issuer' in CONFIG)) {
  console.error('"issuer" configuration missing in config.json')
  process.exit(1)
}

if (typeof CONFIG.issuer !== 'string') {
  console.error('"issuer" configuration must be a string')
  process.exit(1)
}

if (!('alias' in CONFIG)) {
  console.error('"alias" configuration missing in config.json')
  process.exit(1)
}

if (typeof CONFIG.alias !== 'string') {
  console.error('"alias" configuration must be a string')
  process.exit(1)
}

module.exports = CONFIG
