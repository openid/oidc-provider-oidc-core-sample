const { issuer: ISSUER, alias: ALIAS } = require('./get_config')

let baseUrl = ISSUER
if (ISSUER.endsWith('/')) {
  baseUrl = ISSUER.slice(0, -1)
}

const client = {
  client_name: 'first-openid-client',
  scope: 'openid address email phone profile offline_access'
}

const plan = {
  alias: ALIAS,
  description: 'oidc-provider OIDC',
  server: {
    discoveryUrl: baseUrl + '/.well-known/openid-configuration'
  },
  client,
  client2: client,
  resource: {
    resourceUrl: baseUrl + '/me'
  },
  browser: [
    {
      match: baseUrl + '/auth*',
      tasks: [
        {
          task: 'Login',
          optional: true,
          match: baseUrl + '/interaction*',
          commands: [
            ['text', 'name', 'login', 'foo', 'optional'],
            ['text', 'name', 'password', 'bar', 'optional'],
            ['click', 'class', 'login-submit']
          ]
        },
        {
          task: 'Consent',
          optional: true,
          match: baseUrl + '/interaction*',
          commands: [
            ['click', 'class', 'login-submit']
          ]
        },
        {
          task: 'Verify Complete',
          match: `*/test/a/${ALIAS}/callback*`,
          commands: [
            ['wait', 'id', 'submission_complete', 10]
          ]
        }
      ]
    }
  ],
  override: {
    'oidcc-prompt-login': {
      browser: [
        {
          comment: 'updates placeholder during the second authorization',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Login',
              optional: true,
              match: baseUrl + '/interaction*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'Sign-in', 'update-image-placeholder-optional'],
                ['text', 'name', 'login', 'foo', 'optional'],
                ['text', 'name', 'password', 'bar', 'optional'],
                ['click', 'class', 'login-submit']
              ]
            },
            {
              task: 'Consent',
              optional: true,
              match: baseUrl + '/interaction*',
              commands: [
                ['click', 'class', 'login-submit']
              ]
            },
            {
              task: 'Verify Complete',
              match: `*/test/a/${ALIAS}/callback*`,
              commands: [
                ['wait', 'id', 'submission_complete', 10]
              ]
            }
          ]
        }
      ]
    },
    'oidcc-ensure-registered-redirect-uri': {
      browser: [
        {
          comment: 'expect an immediate error page',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Expect redirect uri mismatch error page',
              match: baseUrl + '/auth*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'oops! something went wrong', 'update-image-placeholder']
              ]
            }
          ]
        }
      ]
    },
    'oidcc-ensure-redirect-uri-in-authorization-request': {
      browser: [
        {
          comment: 'expect an immediate error page',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Expect redirect uri mismatch error page',
              match: baseUrl + '/auth*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'oops! something went wrong', 'update-image-placeholder']
              ]
            }
          ]
        }
      ]
    }
  }
}

console.log(JSON.stringify(plan, null, 4))
