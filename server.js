const { readFileSync, existsSync } = require('fs')
const { createServer } = require('https')
const { createHash, randomBytes } = require('crypto')

const { port: PORT, issuer: ISSUER } = require('./get_config')

if (!existsSync('server.key')) {
  console.error('server.key file missing')
  process.exit(1)
}

if (!existsSync('server.crt')) {
  console.error('server.crt file missing')
  process.exit(1)
}

const SERVER_KEY = readFileSync('server.key')
const SERVER_CERTIFICATE = readFileSync('server.crt')

const Koa = require('koa')
const mount = require('koa-mount')
const { Provider } = require('oidc-provider')
const helmet = require('koa-helmet')
const jose = require('jose')
const whitelistedJWA = JSON.parse(JSON.stringify(require('oidc-provider/lib/consts/jwa')))

const { pathname } = new URL(ISSUER)
const app = new Koa()

const ks = new jose.JWKS.KeyStore()
ks.generateSync('RSA', 2048, { use: 'sig' })
ks.generateSync('EC', 'P-256', { use: 'sig' })
ks.generateSync('EC', 'secp256k1', { use: 'sig' })
ks.generateSync('EC', 'P-384', { use: 'sig' })
ks.generateSync('EC', 'P-521', { use: 'sig' })
ks.generateSync('OKP', 'Ed25519', { use: 'sig' })
ks.generateSync('OKP', 'Ed448', { use: 'sig' })

ks.generateSync('RSA', 2048, { use: 'enc' })
ks.generateSync('EC', 'P-256', { use: 'enc' })
ks.generateSync('EC', 'P-384', { use: 'enc' })
ks.generateSync('EC', 'P-521', { use: 'enc' })
ks.generateSync('OKP', 'X25519', { use: 'enc' })
ks.generateSync('OKP', 'X448', { use: 'enc' })

const oidc = new Provider(ISSUER, {
  findAccount (ctx, sub) {
    return {
      accountId: sub,
      claims () {
        return {
          sub,
          address: {
            country: '000',
            formatted: '000',
            locality: '000',
            postal_code: '000',
            region: '000',
            street_address: '000'
          },
          birthdate: '1987-10-16',
          email: 'johndoe@example.com',
          email_verified: false,
          family_name: 'Doe',
          gender: 'male',
          given_name: 'John',
          locale: 'en-US',
          middle_name: 'Middle',
          name: 'John Doe',
          nickname: 'Johny',
          phone_number: '+49 000 000000',
          phone_number_verified: false,
          picture: 'http://lorempixel.com/400/200/',
          preferred_username: 'johnny',
          profile: 'https://johnswebsite.com',
          updated_at: 1454704946,
          website: 'http://example.com',
          zoneinfo: 'Europe/Berlin'
        }
      }
    }
  },
  acrValues: ['urn:mace:incommon:iap:silver'],
  scopes: ['openid', 'offline_access'],
  claims: {
    amr: null,
    address: ['address'],
    email: ['email', 'email_verified'],
    phone: ['phone_number', 'phone_number_verified'],
    profile: ['birthdate', 'family_name', 'gender', 'given_name', 'locale', 'middle_name', 'name',
      'nickname', 'picture', 'preferred_username', 'profile', 'updated_at', 'website', 'zoneinfo']
  },
  cookies: {
    keys: [randomBytes(32)]
  },
  whitelistedJWA,
  async pairwiseIdentifier (ctx, accountId, client) {
    return createHash('sha256')
      .update(client.sectorIdentifier)
      .update(accountId)
      .update('foobar') // put your own unique salt here, or implement other mechanism
      .digest('hex')
  },
  subjectTypes: ['public', 'pairwise'],
  responseTypes: ['code id_token token', 'code id_token', 'code token', 'code', 'id_token token', 'id_token', 'none'],
  jwks: ks.toJWKS(true),
  features: {
    claimsParameter: { enabled: true },
    requestObjects: { request: true },
    encryption: { enabled: true },

    registration: { enabled: true },
    registrationManagement: { enabled: true, rotateRegistrationAccessToken: true },

    frontchannelLogout: { enabled: true },
    backchannelLogout: { enabled: true },
    sessionManagement: { enabled: true }
  },
  clockTolerance: 5
})

{
  const orig = oidc.interactionResult
  oidc.interactionResult = function patchedInteractionResult (...args) {
    if (args[2] && args[2].login) {
      args[2].login.acr = 'urn:mace:incommon:iap:silver'
    }

    return orig.call(this, ...args)
  }
}

app.use(helmet())
app.use(mount(pathname.length > 1 && pathname.endsWith('/') ? pathname.slice(0, -1) : pathname, oidc.app))

const server = createServer({
  key: SERVER_KEY,
  cert: SERVER_CERTIFICATE
}, app.callback())

server.listen(PORT)
